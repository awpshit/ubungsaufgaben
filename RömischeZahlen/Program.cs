﻿Console.Write("Eingabe: "); // 1984
int input = Convert.ToInt32(Console.ReadLine());

NumberToRomanLiterals(100_000, '\u2188');
NumberToRomanLiterals(50_000, '\u2187');
NumberToRomanLiterals(10_000, '\u2182');
NumberToRomanLiterals(5000, '\u2181');
NumberToRomanLiterals(1000, '\u2180');
NumberToRomanLiterals(500, 'D');
NumberToRomanLiterals(100, 'C');
NumberToRomanLiterals(50, 'L');
NumberToRomanLiterals(10, 'X');
NumberToRomanLiterals(5, 'V');
NumberToRomanLiterals(1, 'I');

Console.WriteLine();
Console.WriteLine(@"ↈ");

void NumberToRomanLiterals(int value, char literal)
{
    while (input >= value)
    {
        Console.Write(literal);
        input -= value;
    }
}