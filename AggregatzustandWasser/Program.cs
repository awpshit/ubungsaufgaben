﻿Console.Write("Bitte geben Sie die Temperatur des Wassers ein: ");
float temperature = Convert.ToSingle(Console.ReadLine());
temperature = (float)Math.Round(temperature * 100) / 100;

// Aggregatzustände: Gefroren, Flüssig, Dampf

if (temperature <= 0)
{
    Console.WriteLine("Gefroren");
}
else if (temperature <= 100)
{
    Console.WriteLine("Flüssig");
}
else
{
    Console.WriteLine("Dampf");
}