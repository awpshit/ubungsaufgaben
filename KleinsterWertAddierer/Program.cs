﻿Console.Write("Bitte geben Sie einen Anfangswert an: ");
int min = Convert.ToInt32(Console.ReadLine());

Console.Write("Bitte geben Sie einen Endwert an: ");
int max = Convert.ToInt32(Console.ReadLine());

int sum = 0;
/* Die Schleife fängt bei [min] and und geht solange, bis sie zu [max] angelangt ist */
/* Bei jeder Iteration wird die Variable [i] zu der Variable [sum] addiert. */
for (int i = min; i <= max; i++)
{
    Console.Write($"{i}");
    sum += i;
    /* Sobald [i] den Wert von [max] erreicht hat, wurde das Ende der Berechnung erreicht. */
    /* Somit wird dann eine neue Zeile und ein '='-Zeichen eingefügt. */
    if (i == max)
    {
        Console.WriteLine();
        Console.Write("= ");
    }
    else
    {
        Console.Write(" + ");
    }
}

Console.WriteLine(sum);