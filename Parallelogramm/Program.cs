﻿do
{
    Console.Clear();
    Console.Write("Wie hoch soll das Parallelogramm werden? (standard=5): ");

    string line = Console.ReadLine();
    int height = line.Length != 0 ? Convert.ToInt32(line) : 5;

    Console.Write("Wie breit soll das Parallelogramm werden? (standard=15): ");

    line = Console.ReadLine()!;
    int width = line.Length != 0 ? Convert.ToInt32(line) : 15;

    for (int i = height - 1; i >= 0; i--)
    {
        Console.Write(new string(' ', i));
        Console.Write(new string('*', width));
        Console.WriteLine();
    }

    Console.WriteLine();
    Console.Write("Nochmal? (j/J): ");
} while (Console.ReadKey().Key == ConsoleKey.J);