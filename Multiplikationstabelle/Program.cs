﻿do
{
    Console.Clear();

    int upToNumber;
    do
    {
        Console.Write("Geben Sie die Größe der Tabelle an: ");
        upToNumber = Convert.ToInt32(Console.ReadLine());
        if (upToNumber <= 25)
        {
            break;
        }

        Console.WriteLine("Die Größe muss zwischen 1 und 25 liegen.");
    } while (true);

    int maxProduct = upToNumber * upToNumber;

    int maxProductLength = maxProduct.ToString().Length;

    for (int i = 1; i <= upToNumber; i++)
    {
        for (int j = 1; j <= upToNumber; j++)
        {
            int product = i * j;
            int productLength = maxProductLength - product.ToString().Length;
            if (productLength > 0)
            {
                Console.Write(new string(' ', productLength));
            }

            Console.Write(product);
            Console.Write(" ");
        }

        Console.WriteLine();
    }

    Console.Write("Nochmal? (j/J):");
} while (Console.ReadKey().Key == ConsoleKey.J);