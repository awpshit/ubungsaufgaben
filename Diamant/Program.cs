﻿Console.Write("Wie hoch soll der Diamant werden?: ");
int halfHeight = Convert.ToInt32(Console.ReadLine()) / 2;

for (int i = 0; i < halfHeight; i++)
{
    int amount = (2 * i) + 1;
    string s = new string(' ', halfHeight - i);
    s = s.Insert(s.Length, new string('*', amount));
    Console.WriteLine(s);
}

for (int i = halfHeight; i >= 0; i--)
{
    int amount = (2 * i) + 1;
    string s = new string(' ', halfHeight - i);
    s = s.Insert(s.Length, new string('*', amount));
    Console.WriteLine(s);
}